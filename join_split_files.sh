#!/bin/bash

cat bootimg/01_dtbdump_MT6768.dtb.* 2>/dev/null >> bootimg/01_dtbdump_MT6768.dtb
rm -f bootimg/01_dtbdump_MT6768.dtb.* 2>/dev/null
cat cust/app/customized/partner-com.spotify.music_159/partner-com.spotify.music_159.apk.* 2>/dev/null >> cust/app/customized/partner-com.spotify.music_159/partner-com.spotify.music_159.apk
rm -f cust/app/customized/partner-com.spotify.music_159/partner-com.spotify.music_159.apk.* 2>/dev/null
cat cust/app/customized/partner-com.phonepe.app_227/partner-com.phonepe.app_227.apk.* 2>/dev/null >> cust/app/customized/partner-com.phonepe.app_227/partner-com.phonepe.app_227.apk
rm -f cust/app/customized/partner-com.phonepe.app_227/partner-com.phonepe.app_227.apk.* 2>/dev/null
cat cust/app/customized/partner-com.facebook.katana_91/partner-com.facebook.katana_91.apk.* 2>/dev/null >> cust/app/customized/partner-com.facebook.katana_91/partner-com.facebook.katana_91.apk
rm -f cust/app/customized/partner-com.facebook.katana_91/partner-com.facebook.katana_91.apk.* 2>/dev/null
cat cust/app/customized/partner-com.snapchat.android_200/partner-com.snapchat.android_200.apk.* 2>/dev/null >> cust/app/customized/partner-com.snapchat.android_200/partner-com.snapchat.android_200.apk
rm -f cust/app/customized/partner-com.snapchat.android_200/partner-com.snapchat.android_200.apk.* 2>/dev/null
cat cust/app/customized/partner-com.linkedin.android_44/partner-com.linkedin.android_44.apk.* 2>/dev/null >> cust/app/customized/partner-com.linkedin.android_44/partner-com.linkedin.android_44.apk
rm -f cust/app/customized/partner-com.linkedin.android_44/partner-com.linkedin.android_44.apk.* 2>/dev/null
cat vendor/lib64/librelight_only.so.* 2>/dev/null >> vendor/lib64/librelight_only.so
rm -f vendor/lib64/librelight_only.so.* 2>/dev/null
cat product/data-app/MIUINotes/MIUINotes.apk.* 2>/dev/null >> product/data-app/MIUINotes/MIUINotes.apk
rm -f product/data-app/MIUINotes/MIUINotes.apk.* 2>/dev/null
cat product/data-app/Opera/Opera.apk.* 2>/dev/null >> product/data-app/Opera/Opera.apk
rm -f product/data-app/Opera/Opera.apk.* 2>/dev/null
cat product/data-app/MIMediaEditorGlobal/MIMediaEditorGlobal.apk.* 2>/dev/null >> product/data-app/MIMediaEditorGlobal/MIMediaEditorGlobal.apk
rm -f product/data-app/MIMediaEditorGlobal/MIMediaEditorGlobal.apk.* 2>/dev/null
cat product/priv-app/MIUIMusicGlobal/MIUIMusicGlobal.apk.* 2>/dev/null >> product/priv-app/MIUIMusicGlobal/MIUIMusicGlobal.apk
rm -f product/priv-app/MIUIMusicGlobal/MIUIMusicGlobal.apk.* 2>/dev/null
cat product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null >> product/priv-app/Phonesky/Phonesky.apk
rm -f product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null
cat product/priv-app/Messages/Messages.apk.* 2>/dev/null >> product/priv-app/Messages/Messages.apk
rm -f product/priv-app/Messages/Messages.apk.* 2>/dev/null
cat product/priv-app/MIUISecurityCenterGlobal/MIUISecurityCenterGlobal.apk.* 2>/dev/null >> product/priv-app/MIUISecurityCenterGlobal/MIUISecurityCenterGlobal.apk
rm -f product/priv-app/MIUISecurityCenterGlobal/MIUISecurityCenterGlobal.apk.* 2>/dev/null
cat product/priv-app/ExtraPhotoGlobal/ExtraPhotoGlobal.apk.* 2>/dev/null >> product/priv-app/ExtraPhotoGlobal/ExtraPhotoGlobal.apk
rm -f product/priv-app/ExtraPhotoGlobal/ExtraPhotoGlobal.apk.* 2>/dev/null
cat product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> product/priv-app/MiuiCamera/MiuiCamera.apk
rm -f product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/Gallery_T_Global/Gallery_T_Global.apk.* 2>/dev/null >> product/priv-app/Gallery_T_Global/Gallery_T_Global.apk
rm -f product/priv-app/Gallery_T_Global/Gallery_T_Global.apk.* 2>/dev/null
cat product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null >> product/app/TrichromeLibrary64/TrichromeLibrary64.apk
rm -f product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null
cat product/app/Photos/Photos.apk.* 2>/dev/null >> product/app/Photos/Photos.apk
rm -f product/app/Photos/Photos.apk.* 2>/dev/null
cat product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> product/app/Gmail2/Gmail2.apk
rm -f product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null >> product/app/WebViewGoogle64/WebViewGoogle64.apk
rm -f product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null
cat product/app/MIUIThemeManagerGlobal/MIUIThemeManagerGlobal.apk.* 2>/dev/null >> product/app/MIUIThemeManagerGlobal/MIUIThemeManagerGlobal.apk
rm -f product/app/MIUIThemeManagerGlobal/MIUIThemeManagerGlobal.apk.* 2>/dev/null
cat product/app/YouTube/YouTube.apk.* 2>/dev/null >> product/app/YouTube/YouTube.apk
rm -f product/app/YouTube/YouTube.apk.* 2>/dev/null
cat product/app/Meet/Meet.apk.* 2>/dev/null >> product/app/Meet/Meet.apk
rm -f product/app/Meet/Meet.apk.* 2>/dev/null
cat product/app/Maps/Maps.apk.* 2>/dev/null >> product/app/Maps/Maps.apk
rm -f product/app/Maps/Maps.apk.* 2>/dev/null
cat product/app/MIUIGalleryPlayerService/MIUIGalleryPlayerService.apk.* 2>/dev/null >> product/app/MIUIGalleryPlayerService/MIUIGalleryPlayerService.apk
rm -f product/app/MIUIGalleryPlayerService/MIUIGalleryPlayerService.apk.* 2>/dev/null
cat product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat product/app/MiuiBiometric/MiuiBiometric.apk.* 2>/dev/null >> product/app/MiuiBiometric/MiuiBiometric.apk
rm -f product/app/MiuiBiometric/MiuiBiometric.apk.* 2>/dev/null
cat product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null >> product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk
rm -f product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null >> system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk
rm -f system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
